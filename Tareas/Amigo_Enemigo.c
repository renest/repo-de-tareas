#include <stdio.h>
#include <stdlib.h>

//estructura para todos los nodos. Tendrá doble puntero, para poder "disparar" al nodo anterior y al siguiente.
struct nodo { 
	int vida; //Nos indicará cuanta vida le queda al nodo
	char *valor; //Dira si es un nodo X u 0
	struct nodo *sig; //apuntador siguiente nodo
	struct nodo *ant; //apuntador al anterior 
};

//Estructura para toda la lista.
struct lista{
	struct nodo* inicio; 
};

struct lista* crearLista(){ 
	struct lista *listaTablero = calloc(1,sizeof(struct lista)); //Reserva memoria para la lista.
	return listaTablero;
}

//Función para la creación de cada nodo donde manda a traer memoria para el nodo.
struct nodo* CrearNodo(){
	struct nodo *nuevoNodo = calloc(1,sizeof(struct nodo));
	return nuevoNodo;
}

//Función para insertar un nodo al inicio.
struct lista* insertar(struct lista* listaTablero, int vida,char* nave){
	struct nodo* nuevoNodo = NULL;
	struct nodo* actual = NULL; 

	if (listaTablero == NULL){ 
		return NULL;
	}

	nuevoNodo = CrearNodo();
	nuevoNodo -> valor = nave; //Asigna en el campo de valor nave la nave representada por ese nodo.
	nuevoNodo -> vida = vida; //Asigna en el campo de valor del nodo la vida de la nave.

	if (listaTablero->inicio == NULL){ //Si no ha un nodo ocupando el primer espacio, si está vacío.
		listaTablero->inicio = nuevoNodo; //El nodo nuevo toma la posicion de primero en la lista.
		nuevoNodo->sig = NULL;
		nuevoNodo->ant = NULL;
		actual = nuevoNodo;
	}else{
		actual = listaTablero->inicio;
		while(actual->sig != NULL){ //Este while va a recorrer los nodos hasta que llegue al último.
			actual = actual->sig;
		}
		actual->sig = nuevoNodo;
		nuevoNodo->sig = NULL;
		nuevoNodo->ant = actual;
		actual = nuevoNodo;
	}
	return listaTablero;
}

//Función que permite buscar un elemento, esta función permitirá buscar el elemento para poder disparar.
struct lista* buscar(struct lista* listaTablero, char* busqueda){
	struct nodo* actual = NULL;
	 if (listaTablero == NULL || listaTablero->inicio == NULL){
	 	return NULL;
	 }

	 actual = listaTablero->inicio;

	 while(actual != NULL){
	 	if (actual->valor == busqueda){
	 		return actual;
	 	}
	 	actual = actual->sig;
	 }
	return NULL;
}

//Función para eliminar un nodo si este ya tiene 0 de vida.
struct lista* eliminar(struct lista* listaTablero,char* elemento){
	struct nodo* nuevo_nodo = NULL; 
	struct nodo* actual = NULL; 
	struct nodo* anterior = NULL;
	

	if (listaTablero == NULL){ //Ve si la lista está vacía.
		return NULL;
	}

	nuevo_nodo = CrearNodo();
	nuevo_nodo->valor = elemento;

	// Si la lista esta vacia
	if (listaTablero->inicio == NULL){
		return NULL;
	}

	actual = listaTablero->inicio; //Actual toma la primera posición.
	 
	if (actual->valor == elemento){ //Si actual en el campo de valor es igual al elemento que quiero borrar que está pasando como parametro. Esto es si el que estoy borrando es el primer elemento.
		listaTablero->inicio = actual->sig;

		//Eliminacion del nodo, liberación de la memoria.
		free(actual);
		actual = NULL; 

		return listaTablero;
	}

	anterior = actual; //Me sirve para que no se suelte la lista.
	actual = actual->sig;

	//Validaciones para cuando el nodo a eliminar está después.
	while(actual != NULL){
		if (actual->valor == elemento){
			anterior->sig = actual->sig;
			free(actual);
			actual=NULL;

			return listaTablero;
		}

		anterior = actual;
		actual = actual->sig;
	}

	return NULL;
}

//Función que imprimirá los nodos con su valor.
int imprimirLista(struct lista* listaTablero){
	struct nodo* actual = NULL;

	if (listaTablero == NULL){
		printf("No hay lista\n");
		return -1;
	}
		if (listaTablero->inicio == NULL){
		printf("Lista vacía\n");
		return -2;
	}

	actual = listaTablero->inicio;

	while (actual != NULL){
		printf("<- %s (Vida: %d) -> ", actual->valor,actual->vida); //actual en el campo de valor nos imprimiría la nave, y actual en el campo de vida, la vida de esa. 
		actual = actual->sig;
	}

	printf("\n");
	return 0;
}

int main(int argc, char const *argv[])
{
	struct lista* listaTablero = crearLista();
	struct nodo *actual = NULL;
	struct nodo* busqueda = NULL;
	int opcion = 0;
	int navesJ1 = 3; //Es un contador que nos dirá si el jugador 1 quedó con barcos.
	int navesJ2 = 3; //Es un contador que nos dirá si el jugador 2 quedó con barcos.
	int ganador = 0; //Variable que sirve como booleano para ver si hay ganador y seguir jugando.


	insertar(listaTablero,15,"X1");
	insertar(listaTablero,15,"O1");
	insertar(listaTablero,15,"X2");
	insertar(listaTablero,15,"O2");
	insertar(listaTablero,15,"X3");
	insertar(listaTablero,15,"O3");

	imprimirLista(listaTablero);

	while(ganador==0){
		printf("JUGADOR 1 - X\n-------------------------\n¿Cual nave desea disparar? \n");
		scanf("%d",&opcion);

		switch(opcion){ //Valida 

			case 1:
			busqueda = buscar(listaTablero,"X1");
			//Validación para ver si la nave aún no está eliminada.
			if(busqueda->sig->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->sig->valor == "X1" || busqueda->sig->valor == "X2"|| busqueda->sig->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->sig->valor);//El parametro de elemento, para eliminar, va a ser el que le sigue en la lista.
			}else{
				busqueda->sig->vida = busqueda->sig->vida - 5;
			}
			break;

			case 2:
			busqueda = buscar(listaTablero,"X2");
			//Validación para ver si la nave aún no está eliminada.
			if(busqueda->sig->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->sig->valor == "X1" ||busqueda->sig->valor == "X2"|| busqueda->sig->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->sig->valor);
			}else{
				busqueda->sig->vida = busqueda->sig->vida - 5;
			}
			if(busqueda->ant->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->ant->valor == "X1" ||busqueda->ant->valor == "X2"|| busqueda->ant->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->ant->valor);
			}else{
			busqueda->ant->vida = busqueda->ant->vida - 5;
			}
			break;

			case 3:
			busqueda = buscar(listaTablero,"X3");
			if(busqueda->sig->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->sig->valor == "X1" ||busqueda->sig->valor == "X2"|| busqueda->sig->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->sig->valor);
			}else{
				busqueda->sig->vida = busqueda->sig->vida - 5;
			}

			if(busqueda->ant->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->ant->valor == "X1" ||busqueda->ant->valor == "X2"|| busqueda->ant->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->ant->valor);
			}else{
			busqueda->ant->vida = busqueda->ant->vida - 5;
			}
			break;

			default:
			printf("Nave no válida\n");
			break;
		}

		imprimirLista(listaTablero);

		printf("JUGADOR 2 - O\n-------------------------\n¿Cual nave desea disparar? \n");
		scanf("%d",&opcion);
			
		switch(opcion){

			case 1:
			busqueda = buscar(listaTablero,"O1");
			if(busqueda->sig->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->sig->valor == "X1" ||busqueda->sig->valor == "X2"|| busqueda->sig->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->sig->valor);
			}else{
				busqueda->sig->vida = busqueda->sig->vida - 5;
			}
			if(busqueda->ant->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->ant->valor == "X1" ||busqueda->ant->valor == "X2"|| busqueda->ant->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->ant->valor);
			}else{
			busqueda->ant->vida = busqueda->ant->vida - 5;
			}
			break;

			case 2:
			busqueda = buscar(listaTablero,"O2");
			if(busqueda->sig->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->sig->valor == "X1" ||busqueda->sig->valor == "X2"|| busqueda->sig->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->sig->valor);
			}else{
				busqueda->sig->vida = busqueda->sig->vida - 5;
			}
			if(busqueda->ant->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->ant->valor == "X1" ||busqueda->ant->valor == "X2"|| busqueda->ant->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->ant->valor);
			}else{
			busqueda->ant->vida = busqueda->ant->vida - 5;
			}
			break;

			case 3:
			busqueda = buscar(listaTablero,"O3");
			//Validación para ver si la nave aún no está eliminada.
			if(busqueda->ant->vida == 5){ //La condición será que vida sea 5 para que cuando ya haga ese ultimo disparo elimine al nodo de una vez.
				if(busqueda->ant->valor == "X1" ||busqueda->ant->valor == "X2"|| busqueda->ant->valor == "X3"){
					navesJ1 = navesJ1 - 1;
				}else{
					navesJ2 = navesJ2 - 1;
				}
				eliminar(listaTablero,busqueda->ant->valor);//El parametro de elemento, para eliminar, va a ser el que le sigue en la lista.
			}else{
				busqueda->ant->vida = busqueda->ant->vida - 5;
			}
			break;

			default:
			printf("Nave no válida\n");
			break;
		}
		imprimirLista(listaTablero);

		//Verifica que ya no hayan naves para poder cambiar el estado de la variable ganador y terminar el while.
		if (navesJ1 == 0 || navesJ2 == 0){
			ganador = 1;
		}
	}

	if (navesJ1 == 0){
		printf("El ganador es el jugador 2\n");
	}else{
		printf("El ganador es el jugador 1\n");
	}
	imprimirLista(listaTablero);
	return 0;
}